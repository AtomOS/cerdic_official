# ATOM #

### Sync ###

```bash

# Initialize local repository
repo init -u https://github.com/AtomOrganization/manifest -b ten

# Get Cedric stuff
git clone https://github.com/AtomOrganization/cerdic_official .repo/local_manifests

# Sync
repo sync -c -j$(nproc --all) --force-sync --no-clone-bundle --no-tags
```

### Build ###

```bash

# Set up environment
$ . build/envsetup.sh

# Choose a target
$ lunch aosp_cedric-userdebug

# Build the code
$ mka bacon -j$(nproc --all)
```

To apply for official builds click <a href="https://github.com/AtomOrganization/manifest/blob/ten/apply_for_official.md">HERE</a> for more info
